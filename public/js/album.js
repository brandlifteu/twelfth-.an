app = angular.module('twelfthManApp', ['ngAnimate']);

app.controller('PhotosController', function ($scope, $http, $filter) {
    $scope.showDeleted = 0;
    $scope.selectedItem = 0;
    $scope.selectedTab = 0;
    $scope.setActiveItem = function (item) {
        $scope.selectedItem = item;
    };
    $scope.isActive = function (item) {
        return $scope.selectedItem === item;
    };
    $scope.delete = function () {
        $scope.selectedItem.deleted = 1;
        $http.put('photos/' + $scope.selectedItem.id, $scope.selectedItem)
                .then(
                        function (response) {
                            $scope.selectedItem = 0;
                        },
                        function (response) {
                            $scope.selectedItem.deleted = 0;
                        }
                );

    };
    $scope.restore = function () {
        $scope.selectedItem.deleted = 0;
        $http.put('photos/' + $scope.selectedItem.id, $scope.selectedItem)
                .then(
                        function (response) {
                            $scope.selectedItem = 0;
                        },
                        function (response) {
                            $scope.selectedItem.deleted = 1;
                        }
                );
    };
    $scope.download = function ()
    {
        window.open('download/' + $scope.selectedItem.filename);
    }
    $scope.changeTab = function (tab) {
        $scope.selectedItem = 0;
        $scope.selectedTab = tab;
        $scope.showDeleted = tab;
    };
    $http.get('photos/').
            then(function (response) {

                $scope.photos = response.data;
            });
});