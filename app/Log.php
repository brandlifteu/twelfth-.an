<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = 'log';

    protected $fillable = ['message','type','code'];

    public static function saveData($data) {

        static::create($data);
    }
}
