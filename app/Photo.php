<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public static function saveData($data) {

        static::create($data);
    }

    public static function changeDeletedFlag($id,$deleted) {

        $row = static::find($id);
        $row->deleted = $deleted;
        $row->save();
    }

    public static function getOriginalFilenameByFilename($filename) {

        $row = static::where('filename',$filename)->first();

        if (!isset($row->original_filename)) return false;

        return $row->original_filename;
    }
}
