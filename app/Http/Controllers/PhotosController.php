<?php

namespace App\Http\Controllers;

use App\Log;
use App\Photo;

use Validator;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class PhotosController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        return Photo::All();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

    }

    /**
     * Changing deleted flag
     *
     * @param Request $request
     * @return array
     */
    public function update(Request $request) {

        $all_request = $request->all();

        $validator = Validator::make($all_request, [
            'id' => 'required|numeric',
            'deleted' => 'required|numeric'
        ]);

        if ($validator->fails()) {

            (new LogController())->logError('validator fails at PhotosController@update',20);
        }

        Photo::changeDeletedFlag($all_request['id'],$all_request['deleted']);

        return [
            'success' => true
        ];
    }

    /**
     * Visszaadja az image directory-t
     *
     * @return string
     */
    protected function getImageDir() {

        return base_path('public') . '/images';
    }

    /**
     * Generates image title
     *
     * @param $filename
     * @return string
     */
    protected function generateImageTitleFromFilename($filename) {

        $parts = [];

        foreach (explode('-',pathinfo($filename,PATHINFO_FILENAME)) as $part) {

            $parts[] = ucfirst(strtolower($part));
        };

        return implode(' ',$parts);
    }

    /**
     * Filenevet generál
     *
     * @param $original_filename
     * @return string
     */
    protected function generateFilename($original_filename) {

        $filename = pathinfo($original_filename,PATHINFO_FILENAME);
        $extension = pathinfo($original_filename,PATHINFO_EXTENSION);

        return $filename . '-' . uniqid() . '.' . $extension;
    }

    /**
     * Prepare images to upload
     *
     * @param $images
     * @return array
     */
    public function prepareImagesToUpload($images) {

        $data = [];

        foreach($images as $image) {

            $original_filename = $image->getFilename();

            $data[] = [
                'path' => $image->getRealPath(),
                'original_filename' => $original_filename,
                'filename' => $this->generateFilename($original_filename),
                'title' => $this->generateImageTitleFromFilename($original_filename),
                'deleted' => 0
            ];
        }

        return $data;
    }

    /**
     * Thumbnail nevet generál
     *
     * @param $orig_filename
     * @param $thumb
     * @return string
     */
    protected function generateThumbnailName($orig_filename,$thumb)
    {
        $filename = pathinfo($orig_filename,PATHINFO_FILENAME);
        $extension = pathinfo($orig_filename,PATHINFO_EXTENSION);

        return $filename . '-' . $thumb . '.' . $extension;
    }

    /**
     * Manage image upload
     *
     * @param $image_data
     * @return array
     */
    public function uploadImage($image_data) {

        $image_dir = $this->getImageDir();

        if (!file_exists($image_dir)) mkdir($image_dir);

        $dest = $image_dir . '/' . $image_data['filename'];

        $success = $this->moveImage($image_data['path'],$dest);

        if ($success) {

            unset($image_data['path']);

            $image_data['thumbnail'] = $this->generateThumbnailName($image_data['filename'],'thumb');

            $this->resizeImage($dest,350,250,$image_dir . '/' . $image_data['thumbnail']);

            Photo::saveData($image_data);
        }

        return compact('success');
    }

    /**
     * Move image to final directory
     *
     * @param $from
     * @param $to
     * @return bool
     */
    protected function moveImage($from,$to) {

        if (!copy($from, $to)) {

            (new LogController())->logError('failed to upload image from: ' . $from . ' to: ' . $to,10);

            return false;
        }

        (new LogController())->logMessage('image uploaded from: ' . $from . ' to: ' . $to,10);

        return true;
    }

    /**
     * Resize image
     *
     * @param $path
     * @param $width
     * @param $height
     * @param $dest_filename
     */
    protected function resizeImage($path, $width, $height, $dest_filename) {
        
        $img = Image::make($path)->fit($width, $height);
        
        $img->save($dest_filename, 60);

        $img->destroy();
    }

    /**
     * Downloads image
     *
     * @param $full_path
     * @param $orig_filename
     */
    protected function downloadByFullPath($full_path,$orig_filename) {
        
        if (file_exists($full_path)) {

            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary");
            header("Content-disposition: attachment; filename=\"" . $orig_filename . "\"");
            readfile($full_path);
            die();

        } else {

            die ('File not exists');
        }
    }

    /**
     * Downloads file
     *
     * @param $filename
     * @return array
     */
    public function download($filename) {
        
        $original_filename = Photo::getOriginalFilenameByFilename($filename);

        $success = true;
        $full_path = '';

        if ($original_filename) {

            $image_dir = $this->getImageDir();

            $this->downloadByFullPath($image_dir . '/' . $filename, $original_filename);
        }

        else {

            (new LogController())->logError('Not found file to download: ' . $filename,30);

            $success = false;
        }

        return compact('success','full_path');
    }
}
