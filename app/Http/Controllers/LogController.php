<?php

namespace App\Http\Controllers;

use App\Log;

class LogController extends Controller
{
    public function logError($message,$code) {

        $this->log($message,$code,0);
    }

    public function logMessage($message,$code) {

        $this->log($message,$code,1);
    }

    protected function log($message,$code,$type) {

        Log::saveData(compact('message','code','type'));
    }
}
