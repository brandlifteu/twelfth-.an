<?php

Route::get('/',function(){
    return view('main');
});


Route::resource('photos', 'PhotosController');

Route::get('/download/{filename}','PhotosController@download');

