<div class="album text-muted">
    <div class="container" ng-controller="PhotosController">
        <div id="tabControl">
            <ul class="nav nav-pills">
                <li ng-class="{active: selectedTab === 0}"selectedTab><a href="#" data-ng-click="changeTab(0);">Active</a></li>
                <li ng-class="{active: selectedTab === 1}"><a href="#" data-ng-click="changeTab(1);">Deleted</a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="row">            
            <div class="col-lg-3 col-md-6" ng-repeat="photo in filtered = (photos| filter: { deleted: showDeleted })">
                <div class="card" data-ng-click="setActiveItem(photo);" ng-class="{active: isActive(photo)}">
                    <img ng-src="images/{{photo.thumbnail}}"  data-holder-rendered="true">
                    <p class="card-text text-center">{{ photo.title}}</p>
                </div>
            </div>           
            <div class="no-item" ng-show="filtered.length === 0">
                There is no item in this section.
            </div>
        </div>
        <div id="photosControl" class="container" ng-show="selectedItem !== 0">
            <div class="all" ng-show="selectedItem.deleted == 0">
                <a href="#" title="Delete" data-ng-click="delete()" onfocus="blur()"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                <a href="#" title="Download" data-ng-click="download()" onfocus="blur()"><span class="glyphicon glyphicon-save" aria-hidden="true"></span></a>
            </div>
            <div class="delete" ng-show="selectedItem.deleted == 1">
                <a href="#" title="Restore" data-ng-click="restore()" onfocus="blur()"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span></a>
            </div>
        </div>
    </div>
</div>