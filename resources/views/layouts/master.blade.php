
<!doctype html>
<html lang="en" ng-app="twelfthManApp">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="favicon.ico">

        <title>Album example for Bootstrap</title>

        <!-- Bootstrap core CSS -->   
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/album.min.css" rel="stylesheet">
    </head>

    <body> 
        <main role="main">
            <section class="jumbotron text-center">
                <div class="container">
                    <h1 class="jumbotron-heading">Album example</h1>
                    <p class="lead text-muted">The aim of this test is for you to build a simple Web Application which allows you to preview, download, delete and restore images using your preferred framework/library.</p>   
                </div>
            </section>
            @yield('content')

        </main>
        @include('layouts.footer')
    </body>
</html>
