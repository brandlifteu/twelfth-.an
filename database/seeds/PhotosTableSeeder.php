<?php

use Illuminate\Database\Seeder;
use Illuminate\Filesystem\Filesystem;

class PhotosTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $memory_limit = ini_get('memory_limit');

        if ($memory_limit < 512) {

            $this->showConsoleMessage('maybe your server memory limit is too small... we suggesting 512 megabytes...');
        }

        $images = $this->getImages();

        if (empty($images)) {

            $this->showConsoleMessage('no images found in upload directory...');
            die();
        }

        $PhotosController = new \App\Http\Controllers\PhotosController();

        $prepared_images = $PhotosController->prepareImagesToUpload($images);

        $has_error = false;

        $images_count = count($prepared_images);

        foreach($prepared_images as $key => $image_data) {

            $this->showConsoleMessage('uploading ' . ($key + 1) . '/' . $images_count);

            $upload_res = $PhotosController->uploadImage($image_data);

            if (!$upload_res['success']) $has_error = true;

            gc_collect_cycles();
        }

        if ($has_error) $this->showConsoleMessage('something went wrong... you can check it in Log database table');
        else $this->showConsoleMessage('images uploaded successfully');
    }

    /**
     * Get images from directory
     *
     * @return \Symfony\Component\Finder\SplFileInfo[]
     */
    protected function getImages() {

        $from = base_path('upload');

        return (new Filesystem())->allFiles($from);
    }

    /**
     * Writes message to the console
     *
     * @param $message
     */
    protected function showConsoleMessage($message) {

        echo $message . "\n\r";
    }
}
